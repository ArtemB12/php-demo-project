<?php require('partials/head.php') ?>
<?php require('partials/nav.php') ?>

<main>
    <div class="mx-auto max-w-7xl py-6 sm:px-6 lg:px-8">
        <p class="text-2xl font-bold">You are not authorized to see this page</p>
        <p class="mt-4">
            <a href="/" class="text-blue underline">Back to the Home page</a>
        </p>
    </div>
</main>

<?php require('partials/foot.php') ?>